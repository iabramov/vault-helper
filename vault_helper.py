
""" Vault helper
https://gitlab.com/iabramov/vault-helper
"""

__author__ = "Ivan Abramov"
__license__ = "The MIT License (MIT)"
__version__ = "0.1.1"


import os, sys, subprocess, json, logging, argparse

project_path = os.path.dirname(os.path.abspath(__file__))
log_file_path = project_path+'/vault_helper.log'

if False:
    logging.basicConfig(filename=log_file_path, level=logging.DEBUG)
    logging.getLogger().addHandler(logging.StreamHandler())
else:
    logging.basicConfig(level=logging.DEBUG)

    
env = os.environ.copy()

# For debugging
# env['VAULT_ADDR']='http://127.0.0.1:8200'
# os.environ['VAULT_ADDR']='http://127.0.0.1:8200'

# Revoke a token leaving the token's children
# vault token revoke -mode=orphan
# https://learn.hashicorp.com/vault/operations/ops-generate-root
def exec(command):
    print (command)
    output = subprocess.getoutput(command) # executes commands in a shell
    # output = subprocess.check_output(command.split(' '), env=env,stderr=subprocess.STDOUT).decode('UTF-8') # runs a process
    print (output)

    return output
    

def create_root(args):
    """The operator generate-root command generates a new root token by combining a quorum of share holders. https://www.vaultproject.io/docs/commands/operator/generate-root.html"""
    try:
        logging.info("Generate a one-time password (OTP) to use for XORing the resulting token")
        otp = exec("vault operator generate-root -generate-otp") # -format=json does not work

        logging.info("Initialize a root token generation, providing the OTP code from the step above")
        root_init = json.loads(exec("vault operator generate-root -init -otp=%s -format=json" % otp)) # 
        nonce = root_init['nonce'];

        env['UNSEAL_KEY']=os.environ['UNSEAL_KEY']=args.unseal

        logging.info("The nonce value should be distributed to all unseal key holders. Each unseal key holder provides their unseal key")
        token_json=exec("echo $UNSEAL_KEY | vault operator generate-root -nonce=%s -format=json -" % nonce)
        token_info = json.loads(token_json)
        
        logging.info("When the quorum of unseal keys are supplied, the final user will also get the encoded root token")
        decoded_token = exec("vault operator generate-root -decode=%s -otp=%s" % (token_info['encoded_token'], otp))

        print("A new root token: %s" % decoded_token)

    except subprocess.CalledProcessError as exc:
        logging.error("Error : %s %s", int(exc.returncode), exc.output)

def cancel_generation(args):
    """ Resets the root token generation progress. This will discard any submitted unseal keys or configuration. https://www.vaultproject.io/docs/commands/operator/generate-root.html#cancel """
    try:
        os.system("vault operator generate-root -cancel")
    except subprocess.CalledProcessError as exc:
        logging.error("Error : %s %s", int(exc.returncode), exc.output)

def revoke_root(args):
    """ Revokes a token leaving the token's children. https://www.vaultproject.io/docs/commands/token/revoke.html """
    try:
        os.system("vault token revoke -mode=orphan %s" % args.root_token)
    except subprocess.CalledProcessError as exc:
        logging.error("Error : %s %s", int(exc.returncode), exc.output)

parser = argparse.ArgumentParser(description="Vault helper.", usage='python vault_helper.py')
subparsers = parser.add_subparsers()

# args without "-" are values
# create the parser for the "split" command
parser_create_root = subparsers.add_parser('create_root')
# parser_create_root.add_argument('root_token', type=str, help='Root token')
parser_create_root.add_argument('unseal', type=str, help='Unseal key')
parser_create_root.set_defaults(func=create_root)

parser_revoke_root = subparsers.add_parser('cancel_generation')
parser_revoke_root.set_defaults(func=cancel_generation)

parser_revoke_root = subparsers.add_parser('revoke_root')
parser_revoke_root.add_argument('root_token', type=str, help='Root token')
parser_revoke_root.set_defaults(func=revoke_root)

if len(sys.argv)==1:
    parser.print_help(sys.stderr)
else:
    args = parser.parse_args()
    if hasattr(args, 'func'):
        args.func(args)
