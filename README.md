# Vault Helper
Python script that helps to manipulate a Vault root token.

## Requirements
- Python3

## Usage
Run ```python3 vault_helper.py -h``` to see the supported command line arguments.

The operator generate-root command generates a new root token by combining a quorum of share holders. Read the official [documentation](https://www.vaultproject.io/docs/commands/operator/generate-root.html) for details.
```bash
python vault_helper.py create_root <UNSEAL_KEY>
```

Resets the root token generation progress. This will discard any submitted unseal keys or configuration. Take a look at the official [documentation](https://www.vaultproject.io/docs/commands/operator/generate-root.html#cancel) for details.
```bash
python vault_helper.py cancel_generation
```

Revokes a token leaving the token's children. Read the official [documentation](https://www.vaultproject.io/docs/commands/token/revoke.html) for details.
```bash
python vault_helper.py revoke_root <ROOT_TOKEN>
```

